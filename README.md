# Welcome to NoButterknife!
To be 100% clear, I have nothing against [Butterknife](http://jakewharton.github.io/butterknife/), but I do believe it's time may have come.
Some of the problems it solved are no longer quite so relevant.
For example:

* `findViewById()` no longer requires a cast.
* With Java 8 or Kotlin, it is now trivial to define callbacks using lambdas.
* Various conveniences can be replaced with Kotlin extension functions.

And who wants to deal with unbinding, anyway?

## What's the big deal?
Annotation processing is slooow.
Butterknife has had an [open PR](https://github.com/JakeWharton/butterknife/pull/1230) for enabling incremental annotation processing since March 24, 2018, but there is no evidence it's going to be merged anytime soon.
There is an alternative, the Butterknife-reflect runtime, but it doesn't support `@Nullable` fields, and so is not feature-complete when compared to the codegen version.

## What to do about it?
Well, stop using it. Replace all those usages with boring old `findViewById()`, or maybe the [kotlin-android-extensions](https://kotlinlang.org/docs/tutorials/android-plugin.html) if you're feeling spunky.
But wait! You have a large legacy codebase, and it's going to be a real pain to make all those changes all at once.
The NoButterknife plugin to the rescue.
Simply apply the plugin, apply minimal configuration, and occasionally run the `noButterknife` task.
It will fail your build if it detects that the number classes that use Butterknife has increased.
This helps ensure that your usage of Butterknife diminishes over time, and keeps those new devs on your team from inadvertently adding new ones ;-)

## How to use
The NoButterknife plugin will add a task to your project, `noButterknife`.
Run this task every so often.
If it succeeds, then your usage of Butterknife is either stable or has diminished since the last run.

By default, NoButterknife will look in `src/main/java`. You can configure this. For example:

```groovy
nobutterknife {
    // Add Kotlin source
    source += "src/main/kotlin"
}
```

Also by default, it creates a report at `${project.buildDir}/nobutterknife/report.txt`.
This will obviously be of limited utility in the presence of a `clean`, so you should configure this:

```groovy
nobutterknife {
    output = new File("some/stable/path", "report.txt")
}
```

You may then do what you like with that file.

## Installation
```groovy
buildscript {
    repositories {
        gradlePluginPortal()
    }
    dependencies {
        classpath 'com.autonomousapps:nobutterknife:0.5'
    }
}

apply plugin: 'nobutterknife'
```