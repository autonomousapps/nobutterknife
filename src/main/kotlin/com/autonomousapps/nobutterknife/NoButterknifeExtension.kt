package com.autonomousapps.nobutterknife

import org.gradle.api.Project
import org.gradle.api.file.FileTree
import java.io.File

open class NoButterknifeExtension(project: Project) {

    var source: FileTree = project.fileTree("src/main/java")

    var output: File = project.file("${project.buildDir}/nobutterknife/report.txt")
}