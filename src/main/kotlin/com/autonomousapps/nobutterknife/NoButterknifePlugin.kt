package com.autonomousapps.nobutterknife

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.register

class NoButterknifePlugin : Plugin<Project> {

    override fun apply(target: Project): Unit = target.run {
        val extension = extensions.create<NoButterknifeExtension>("nobutterknife", this)

        tasks.register<NoButterknifeTask>("noButterknife") {
            group = "verification"
            description = "Run NoButterknifePlugin analysis for java classes"

            source = extension.source
            reportFile = extension.output
        }
    }
}