package com.autonomousapps.nobutterknife

import org.gradle.api.GradleException
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.SourceTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.nio.charset.Charset

open class NoButterknifeTask : SourceTask() {

    @OutputFile
    lateinit var reportFile: File

    @TaskAction
    fun run() {
        val oldCount =
            if (reportFile.exists()) reportFile.readText(Charset.forName("UTF-8")).toInt()
            else Integer.MAX_VALUE

        val count = source.files.parallelStream()
            .filter { it.readText().contains("import butterknife") }
            .mapToInt { 1 }.sum()

        if (count > oldCount) {
            throw GradleException("Stop using Butterknife! Usages increased by ${count - oldCount}")
        }

        reportFile.writeText("$count")
    }
}
