plugins {
    `kotlin-dsl`
    `maven-publish`
}

group = "com.autonomousapps"
version = "0.5"

gradlePlugin {
    plugins {
        register("nobutterknife") {
            id = "nobutterknife"
            implementationClass = "com.autonomousapps.nobutterknife.NoButterknifePlugin"
        }
    }
}

publishing {
    repositories {
        maven(url = "build/repository")
    }
}

repositories {
    google()
    jcenter()
}

dependencies {
    implementation("com.android.tools.build:gradle:3.2.1")
}

//import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
//
//plugins {
//    kotlin("jvm") version "1.3.11"
//}
//
//group = "com.autonomousapps"
//version = "0.1-SNAPSHOT"
//
//repositories {
//    mavenCentral()
//}
//
//dependencies {
//    compile(kotlin("stdlib-jdk8"))
//}
//
//tasks.withType<KotlinCompile> {
//    kotlinOptions.jvmTarget = "1.8"
//}